/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogi.controller;


import com.blogi.model.blogService;
import com.blogi.model.blogServiceQualifier;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;


/**
 *
 * @author Opiframe
 */

@Named(value = "blogBean")
@SessionScoped
public class blogBean implements Serializable {

 
    @Inject @blogServiceQualifier private blogService blogService;

    
    @Resource UserTransaction trans;
    @PersistenceContext EntityManager eManageri;
    
    public blogBean() {
       
    }
    @PostConstruct
    public void init(){
        //blogService.noudaDataKannasta();
        try {
            //this.blogikirjoitukset = new ArrayList<>();
            System.out.println("Haetaan artikkelit kannasta!");
            trans.begin();
            blogService.setBlogikirjoitukset(eManageri.createQuery("Select e from artikkeli e").getResultList());
            trans.commit();
            System.out.println("Artikkelit haettu onnistuneesti!");
        }
        catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException e) {
            System.out.println(e.getMessage());
        }
    }
    /**
     * @return the blogService
     */
    public blogService getBlogService() {
        return blogService;
    }

    /**
     * @param blogService the blogService to set
     */
    public void setBlogService(blogService blogService) {
        this.blogService = blogService;
    }         
}
