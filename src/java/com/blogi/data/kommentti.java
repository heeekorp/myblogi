/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogi.data;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Opiframe
 */
@Entity
public class kommentti implements Serializable {
    private String kommenttiTeksti;
    private String kommentoijanNimi;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne()
    private artikkeli artikkeli;

    public kommentti(){}
    
    public kommentti(String kommenttiTeksti, String kommentoijanNimi) {
        this.kommenttiTeksti = kommenttiTeksti;
        this.kommentoijanNimi = kommentoijanNimi;
    }
    
    /**
     * @return the kommenttiTeksti
     */
    public String getKommenttiTeksti() {
        return kommenttiTeksti;
    }

    /**
     * @param kommenttiTeksti the kommenttiTeksti to set
     */
    public void setKommenttiTeksti(String kommenttiTeksti) {
        this.kommenttiTeksti = kommenttiTeksti;
    }

    /**
     * @return the kommentoijanNimi
     */
    public String getKommentoijanNimi() {
        return kommentoijanNimi;
    }

    /**
     * @param kommentoijanNimi the kommentoijanNimi to set
     */
    public void setKommentoijanNimi(String kommentoijanNimi) {
        this.kommentoijanNimi = kommentoijanNimi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the artikkeli
     */
    public artikkeli getArtikkeli() {
        return artikkeli;
    }

    /**
     * @param artikkeli the artikkeli to set
     */
    public void setArtikkeli(artikkeli artikkeli) {
        this.artikkeli = artikkeli;
    }
}
