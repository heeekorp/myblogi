/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogi.data;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

/**
 *
 * @author Opiframe
 */
@Entity
public class artikkeli implements Serializable {    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Lob
    @Column(name="TEKSTI", length=9999)
    private String teksti;
    private String otsikko;
    private String kirjottaja;
    @OneToMany(orphanRemoval=true, mappedBy = "artikkeli")
    private List<kommentti>kommentit;

    
    public artikkeli(){}
    
    public artikkeli(String otsikko, String teksti, String kirjottaja) {
        this.teksti = teksti;
        this.otsikko = otsikko;
        this.kirjottaja = kirjottaja;
        this.kommentit = new ArrayList<>();
    }

    /**
     * @return the teksti
     */
    public String getTeksti() {
        return teksti;
    }

    /**
     * @param teksti the teksti to set
     */
    public void setTeksti(String teksti) {
        this.teksti = teksti;
    }

    /**
     * @return the kommentit
     */
    public List<kommentti> getKommentit() {
        return kommentit;
    }

    /**
     * @param kommentit the kommentit to set
     */
    public void setKommentit(List<kommentti> kommentit) {
        this.kommentit = kommentit;
    }

    /**
     * @return the otsikko
     */
    public String getOtsikko() {
        return otsikko;
    }

    /**
     * @param otsikko the otsikko to set
     */
    public void setOtsikko(String otsikko) {
        this.otsikko = otsikko;
    }

    /**
     * @return the kirjoittaja
     */
    public String getKirjottaja() {
        return kirjottaja;
    }

    /**
     * @param kirjottaja
     */
    public void setKirjottaja(String kirjottaja) {
        this.kirjottaja = kirjottaja;
    }
    
    public void lisaaKommentti(kommentti k){
        this.kommentit.add(k);
        
        
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return "com.blogi.artikkeli[ id=" + id + " ]";
    }
}
