/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogi.model;

import com.blogi.data.artikkeli;
import com.blogi.data.kommentti;
import static com.google.common.base.Charsets.ISO_8859_1;
import static com.google.common.base.Charsets.UTF_8;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;


/**
 *
 * @author Opiframe
 */
@Named(value = "blogService")
@SessionScoped
public class blogService implements Serializable {

    private List<artikkeli>blogikirjoitukset;
    private artikkeli NaytettavaArtikkeli;
    
    private String kommenttiTeksti;
    private String kommentoijanNimi;
    
    private String uusiTeksti;
    private String uusiOtsikko;
    private String uusiKirjoittaja;
    
    UserTransaction trans;
    EntityManager eManageri;
    
    /**
     * Creates a new instance of blogService
     * @param userTransaction
     * @param entityManager
     */
    public blogService(UserTransaction userTransaction, EntityManager entityManager) {
        this.trans = userTransaction;
        this.eManageri = entityManager;
        this.blogikirjoitukset = new ArrayList<>();
        NaytettavaArtikkeli = null;
        
        kommentoijanNimi = "";
        kommenttiTeksti = "";
        uusiKirjoittaja = "";
        uusiOtsikko = "";
        uusiTeksti = "";
    }
    
   
      /**
     * @return the blogikirjoitukset
     */
    public List<artikkeli> getBlogikirjoitukset() {
        return blogikirjoitukset;
    }

    /**
     * @param blogikirjoitukset the blogikirjoitukset to set
     */
    public void setBlogikirjoitukset(List<artikkeli> blogikirjoitukset) {
        this.blogikirjoitukset = blogikirjoitukset;
    }
    
    public String naytaArtikkeli(artikkeli a){
        setNaytettavaArtikkeli(a);
        System.out.println("Näytetään artikkeli:" + getNaytettavaArtikkeli().getOtsikko());
        return "naytakirjoitus";
    }
    
    public String muokkaaArtikkeli(artikkeli a){
        setNaytettavaArtikkeli(a);
        System.out.println("Muokataan artikkeli:" + getNaytettavaArtikkeli().getOtsikko());
        return "muokkaakirjoitus";
    }
    
    public void lisaaKommentti(){
        NaytettavaArtikkeli.lisaaKommentti(new kommentti(kommenttiTeksti, kommentoijanNimi));
        System.out.println("Lisätään kommetti by: " + kommentoijanNimi);
        
        kommentti k = new kommentti();
        k.setArtikkeli(NaytettavaArtikkeli);
        k.setKommenttiTeksti(kommenttiTeksti);
        k.setKommentoijanNimi(kommentoijanNimi);
        
        try {
            System.out.println("Tallenetaan uusi kommentti kantaan!");
            trans.begin();
            eManageri.persist(k);
            trans.commit();
            System.out.println("Tallennus onnistui!");
        }
        catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException e) {
        System.out.println(e.getMessage());
        }
        
        kommentoijanNimi = "";
        kommenttiTeksti = "";

    }
    
    public String lisaaArtikkeli(){
        byte ptext[] = uusiOtsikko.getBytes(ISO_8859_1); 
        String Otsikko = new String(ptext, UTF_8); 
        byte ptext2[] = uusiTeksti.getBytes(ISO_8859_1); 
        String Teksti = new String(ptext2, UTF_8);
        byte ptext3[] = uusiKirjoittaja.getBytes(ISO_8859_1); 
        String Kirjoittaja = new String(ptext3, UTF_8);
        
        artikkeli uusiArtikkeli = new artikkeli();
        uusiArtikkeli.setOtsikko(Otsikko);
        uusiArtikkeli.setTeksti(Teksti);
        uusiArtikkeli.setKirjottaja(Kirjoittaja);
        //Integer dummy = blogikirjoitukset.size() + 1;
        //uusiArtikkeli.setId(dummy.longValue());
        
        blogikirjoitukset.add(uusiArtikkeli);
              
        try {
                System.out.println("Tallenetaan uusi artikkeli kantaan!");
                trans.begin();
                eManageri.persist(uusiArtikkeli);
                trans.commit();
                System.out.println("Tallennus onnistui!");
            }
            catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException e) {
            System.out.println(e.getMessage());
        }

        
        uusiKirjoittaja = "";
        uusiOtsikko = "";
        uusiTeksti = "";
        return "index";
    }
    
    public String poistaArtikkeli(){     
        blogikirjoitukset.remove(NaytettavaArtikkeli);
        
        try{
            artikkeli aa = eManageri.find(artikkeli.class, NaytettavaArtikkeli.getId());
            trans.begin();
            aa = eManageri.merge(aa);
            eManageri.remove(aa);
            trans.commit();
        }catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException e) {
            System.out.println(e.getMessage());
        }
        
        uusiKirjoittaja = "";
        uusiOtsikko = "";
        uusiTeksti = "";
        return "index";
    }
    
    public String tallennaMuokattavaArtikkeli(){
        byte ptext[] = NaytettavaArtikkeli.getOtsikko().getBytes(ISO_8859_1); 
        NaytettavaArtikkeli.setOtsikko(new String(ptext, UTF_8)); 
        byte ptext2[] = NaytettavaArtikkeli.getTeksti().getBytes(ISO_8859_1); 
        NaytettavaArtikkeli.setTeksti(new String(ptext2, UTF_8));
        byte ptext3[] = NaytettavaArtikkeli.getKirjottaja().getBytes(ISO_8859_1); 
        NaytettavaArtikkeli.setKirjottaja(new String(ptext3, UTF_8));
        
        try {
                System.out.println("Tallenetaan muokattu artikkeli kantaan!");
                artikkeli aa = eManageri.find(artikkeli.class, NaytettavaArtikkeli.getId());
                trans.begin();
                aa.setOtsikko(NaytettavaArtikkeli.getOtsikko());
                aa.setTeksti(NaytettavaArtikkeli.getTeksti());
                aa.setKirjottaja(NaytettavaArtikkeli.getKirjottaja());
                eManageri.merge(aa);
                trans.commit();
                System.out.println("Tallennus onnistui!");
            }
            catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException e) {
            System.out.println(e.getMessage());
        }

        return "index";
    }

    /**
     * @return the NaytettavaArtikkeli
     */
    public artikkeli getNaytettavaArtikkeli() {
        return NaytettavaArtikkeli;
    }

    /**
     * @param NaytettavaArtikkeli the NaytettavaArtikkeli to set
     */
    public void setNaytettavaArtikkeli(artikkeli NaytettavaArtikkeli) {
        this.NaytettavaArtikkeli = NaytettavaArtikkeli;
    }

    /**
     * @return the kommenttiTeksti
     */
    public String getKommenttiTeksti() {
        return kommenttiTeksti;
    }

    /**
     * @param kommenttiTeksti the kommenttiTeksti to set
     */
    public void setKommenttiTeksti(String kommenttiTeksti) {
        this.kommenttiTeksti = kommenttiTeksti;
    }

    /**
     * @return the kommentoijanNimi
     */
    public String getKommentoijanNimi() {
        return kommentoijanNimi;
    }

    /**
     * @param kommentoijanNimi the kommentoijanNimi to set
     */
    public void setKommentoijanNimi(String kommentoijanNimi) {
        this.kommentoijanNimi = kommentoijanNimi;
    }

    /**
     * @return the uusiTeksti
     */
    public String getUusiTeksti() {
        return uusiTeksti;
    }

    /**
     * @param uusiTeksti the uusiTeksti to set
     */
    public void setUusiTeksti(String uusiTeksti) {
        this.uusiTeksti = uusiTeksti;
    }

    /**
     * @return the uusiOtsikko
     */
    public String getUusiOtsikko() {
        return uusiOtsikko;
    }

    /**
     * @param uusiOtsikko the uusiOtsikko to set
     */
    public void setUusiOtsikko(String uusiOtsikko) {
        this.uusiOtsikko = uusiOtsikko;
    }

    /**
     * @return the uusiKirjoittaja
     */
    public String getUusiKirjoittaja() {
        return uusiKirjoittaja;
    }

    /**
     * @param uusiKirjoittaja the uusiKirjoittaja to set
     */
    public void setUusiKirjoittaja(String uusiKirjoittaja) {
        this.uusiKirjoittaja = uusiKirjoittaja;
    }
    
    public List<kommentti> getKommentit() {
        return getKommentit();
    }
    
}
