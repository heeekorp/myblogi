/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blogi.model;

import java.io.Serializable;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

/**
 *
 * @author Opiframe
 */
@SessionScoped
public class blogServiceFactory implements Serializable{
    private blogService blogService;

    @Resource private UserTransaction trans;
    @PersistenceContext private EntityManager eManageri;
    
    @Produces
    @blogServiceQualifier
    
    public blogService getblogService(){
        if (blogService != null) {
            return blogService;
        }
        blogService = new blogService(trans, eManageri);
        return blogService;       
    }


}
